<?php

namespace Drupal\crawler_migrate\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements an ConfigForm form.
 */
class ConfigForm extends FormBase {

  /**
   * Drupals config container.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal message interface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Settings key.
   *
   * @var string
   */
  protected $settingsKey = 'crawler_migrate.config';

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return str_replace('.', '_', $this->settingsKey);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->settingsKey);

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Crawler base url'),
      '#description' => $this->t('Include schema, do not include trailing slash. eg http://crawler.doghouse.agency'),
      '#default_value' => $config->get('base_url'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key for the API'),
      '#description' => $this->t('Key supllied by Crawler'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if (!UrlHelper::isValid($values['base_url'], TRUE)) {
      $form_state->setErrorByName('base_url', 'Invalid URL');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configFactory->getEditable($this->settingsKey)
      ->set('base_url', $values['base_url'])
      ->set('api_key', $values['api_key'])
      ->save();

    $this->messenger->addStatus($this->t('Settings have been saved'));
  }

}
