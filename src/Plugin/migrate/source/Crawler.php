<?php


namespace Drupal\crawler_migrate\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;

/**
 * Source plugin for retrieving data via Crawler.
 *
 * @MigrateSource(
 *   id = "crawler"
 * )
 */
class Crawler extends Url {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $config = crawler_migrate_settings();

    // Build the full endpoint url.
    $base_url = $config->get('base_url');
    $url = $base_url . $configuration['endpoint'];

    // Add to parent Url class config.
    $configuration['urls'] = [$url];

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }
}
