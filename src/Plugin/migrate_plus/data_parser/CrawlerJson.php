<?php

namespace Drupal\crawler_migrate\Plugin\migrate_plus\data_parser;

use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;

/**
 * Obtain JSON data for migration.
 *
 * @DataParser(
 *   id = "crawler_json",
 *   title = @Translation("Crawler JSON")
 * )
 *
 * Settings:
 * - Same as `json` plugin
 * - Additional `content_type` attribute that filters by that content type
 */
class CrawlerJson extends Json {

  /**
   * Iterator over the JSON data.
   *
   * @var \Iterator
   */
  protected $iterator;

  /**
   * Retrieves the JSON data and returns it as an array.
   *
   * @param string $url
   *   URL of a JSON feed.
   *
   * @return array
   *   The selected data to be iterated.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   */
  protected function getSourceData($url) {
    $source_data = parent::getSourceData($url);

    $key = $this->configuration['filter_key'];
    $val = $this->configuration['filter_value'];
    if (!empty($key) && !empty($val) && !empty($source_data)) {
      $source_data = $this->filterContent($source_data, $key, $val);
    }

    return $source_data;
  }

  /**
   * Filter items from an array based on key and value match.
   *
   * Eg. A list may have an attribute 'content_type' and a possible values of
   * 'page', 'article', 'user'. Using this you could get all items which have
   * the 'content_type' of 'page'.
   *
   * There is probably a more elegant way of doing this.
   *
   * @param array $items
   * @param $key
   * @param $value
   *
   * @return array
   */
  protected function filterContent(array $items, $key, $value) {
    $out = [];
    $used = [];

    foreach ($items as $k => $item) {
      // Avoid duplicate items.
      if (in_array($k, $used)) {
        continue;
      }

      // If key and value match add to output, else exclude.
      foreach ($item['attributes'] as $prop => $val) {
        if ($key === $prop && $val === $value) {
          $out[] = $item;
          $used[] = $k;
        }
      }
    }
    return $out;
  }

}
