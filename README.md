# Content Crawler Migration 

This Drupal 8 module is a companion module to the [Crawler project](https://bitbucket.org/doghouseagency/crawler/src). 
It provides a structure tailored towards migrating content from the Crawler platform along with helper migrate plugins 
(parsers, source, process, etc).

It also provides a migrate group called `crawler_migrate` which all other migrations should use. This keeps most of the 
common migrate settings in a single place (this module) and extending modules require less effort to manage.

The migration is centered around a remote url source with a JSON structure. If you are not using the crawler as a 
data source, this module might still be useful for examples of how things are done.

## Dependencies 

The following modules are required for this to work.

* [migrate_plus](https://www.drupal.org/project/migrate_plus)
* [migrate_tools](https://www.drupal.org/project/migrate_plus)

## Optional dependencies 

The following modules make it easier to develop your migration

* [Config Devel](https://www.drupal.org/project/config_devel) - Allows easily re-importing configurations, specifically
  migration configurations which are all stored in the database.
* [Migrate Devel](https://www.drupal.org/project/migrate_devel) - Allows adding `--migrate-debug` flag to migration
  drush commands so you can see extra debug info.

## Installation

* Ensure you have `https://packages.dogouse.agency` as a composer repo
* Run `composer require doghouse/crawler_migrate`
* Run `drush en crawler_migrate`
* Visit the config page `/admin/config/services/crawler-migrate` and add a base url and api key (get from Crawler)
* Extend the module with your own, see below.

## Extending this module

This module should only contain shared and generic code and doesn't do much by itself. Your migration WILL need an 
additional custom module to add your specific migration files to.  

### Your module structure

The following is an example structure for your new module.

```
my_migrate_module
  |-- config
  |      |-- install
  |             |-- migrate_plus.migration.MIGRATION_TYPE.yml
  |             |-- migrate_plus.migration.MIGRATION_TYPE.yml
  |             |-- migrate_plus.migration.MIGRATION_TYPE.yml
  |-- my_migrate_module.info  
``` 

In the above example, each one of your migrations will have a different `MIGRATION_TYPE`. These may be for different
content or entity types. An example might be `migrate_plus.migration.pages.yml` or 
`migrate_plus.migration.images.yml`.

### Migration YML file structure

The following is an example of what your migration configs look like

```
id: pages
label: Node > Pages migration

# Important, don't change.
migration_group: crawler_migrate 

source:
  # Important, don't change.
  plugin: crawler
  
  # Important, get from Crawler site.
  endpoint: /site/1/output/all

  # Define all the remote bits of data you want for each row. All parsed content from
  # crawler will under the attributes property. Eg attributes/body.
  fields:
    -
      name: id
      label: 'Unique ID'
      selector: id
    -
      name: title
      label: 'Title'
      selector: attributes/title
    -
      name: body
      label: 'Body'
      selector: attributes/body
  
  # Define the mappings, local fields and properties are mapped to fields above.    
  process:
    id: id
    title: title
    body/value: body
    body/format:
      plugin: default_value
      default_value: rich_text   
    type:
      plugin: default_value
      default_value: page  
           
 ## Defaults for destination.
  destination:
    plugin: 'entity:node'
    default_bundle: page           
```

The main thing that will change per migration is the `endpoint`, get this from Crawler. Of course fields and process
are specific to what you are migrating.

Check the `examples` folder for more migrate yml examples.

### Re-importing config after YML changes

Enable the [Config Devel](https://www.drupal.org/project/config_devel) then in your module, add something similar to 
the following 

```
config_devel:
  install:
  - migrate_plus.migration.pages
  - migrate_plus.migration.images
```

Then after any change to your YML, simply run `drush cdi MY_MODULE_NAME` and it will re-import your YML.

## Run a migration

You can use the UI in Drupal but `drush` is usually better. Handy commands:

* `drush migrate:status --group=crawler_migrate` - list status and count of all migrations.
* `drush migrate:import --group=crawler_migrate` - import all
* `drush migrate:rollback --group=crawler_migrate` - rollback all migrations
* `drush migrate:stop --group=crawler_migrate` - stop all migrations
* `drush migrate:reset-status --group=crawler_migrate` - reset all migrations
